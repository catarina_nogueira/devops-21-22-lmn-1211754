# CA5/Part2 -Continuous Integration and Continuous Delivery Pipelines     

## Attempting a Pipeline with GitLabs          

### By Catarina Nogueira             


## 1.Introduction to Continuous Integration and Continuous Delivery

**Continuous integration** is a DevOps software development practice where developers regularly merge their code changes           
into a central repository, after which automated builds and tests are run.               
Continuous integration most often refers to the build or integration stage of the software release process and entails             
both an automation component (e.g. a CI or build service) and a cultural component (e.g. learning to           
integrate frequently). The key goals of continuous integration are to find and address bugs quicker,           
improve software quality, and reduce the time it takes to validate and release new software updates.            

**Continuous delivery** is a software development practice where code changes are automatically              
prepared for a release to production.                   
It expands upon continuous integration by deploying all code changes to a testing environment        
and/or a production environment after the build stage.       
When properly implemented, developers will always have a deployment-ready build artifact         
that has passed through a standardized test process.           
**Continuous delivery** lets developers automate testing beyond just unit tests, so they can verify application updates     
across multiple dimensions before deploying to customers. These tests may include UI testing, load testing,           
integration testing, API reliability testing, etc. This helps developers more thoroughly validate updates and         
pre-emptively discover issues. With the cloud, it is easy and cost-effective to automate the creation and         
replication of multiple environments for testing, which was previously difficult to do on-premises.           


## 2.CI/CD Pipeline
A continuous integration and continuous deployment (CI/CD) pipeline is a series of steps that must be performed in        
order to deliver a new version of software.          
CI/CD pipelines are a practice focused on improving software delivery throughout the software development          
life cycle via automation.         
By automating CI/CD throughout development, testing, production, and monitoring phases of the software development     
lifecycle, organizations are able to develop higher quality code, faster. Although it’s possible to manually execute              
each of the steps of a CI/CD pipeline, the true value of CI/CD pipelines is realized through automation.              

A pipeline is a process that drives software development through a path of building, testing, and deploying code,           
also known as CI/CD. By automating the process, the objective is to minimize human error and maintain a consistent        
process for how software is released. Tools that are included in the pipeline could include compiling code, unit tests,            
code analysis, security, and binaries creation. For containerized environments, this pipeline would also include               
packaging the code into a container image to be deployed across a hybrid cloud.            


## 3. CI/CD tools           
There are several CI/CD tool such as:            
- Jenkins;         
- CircleCI;             
- AWS CodeBuild;           
- Azure DevOps;             
- Atlassian Bamboo;           
- Argo CD;            
- Buddy;              
- Drone;             
- Travis CI;                


## 4.Jenkins                

**Jenkins** is an open source CI tool. It helps automate the parts of software development related to building,            
testing, and deploying, facilitating continuous integration and continuous delivery. It is a server-based system that              
runs in servlet containers such as Apache Tomcat. It supports version control tools, including AccuRev, CVS,            
Subversion, Git, Mercurial, Perforce, ClearCase and RTC, and supports build system such as Ant, Maven or Gradle.            

A Jenkins pipeline script is written in Groovy.                          

Jenkins users define their pipelines in a Jenkinsfile that describes different stages such as build, test, and deploy.            
Environment variables, options, secret keys, certifications, and other parameters are declared in the file and then             
referenced in stages.          
The post section handles error conditions and notifications.               

There are some core concepts to Jenkins, namely:                 
- Job: Build Jobs are the core of the Jenkins build process; A process can be a complete pipeline or a specific task;           
- Node: Creates a "workspace" where the Jenkins build the project. It also schedules build steps on a build agent;           
- Stage: It describes the build steps necessary for each stage action;            
- Step: A single task (or command). It provides instructions to Jenkins;                   


## 5.GitLab           

GitLab is an open source code repository and collaborative software development platform for large DevOps and DevSecOps                 
projects. GitLab is free for individuals.         
Gitlab is a self-hosted software that offers many functionalities such as code reviews, activity feeds,             
repository management, and issue tracking.             

Like Jenkins, GitLab helps automate the parts of software development related to building,            
testing, and deploying, facilitating continuous integration and continuous delivery.             


## 6.Attempting Implementation              

**Step1:** Go to www.gitlab.com           
- create a free trial account          

**Step2:** Go to menu             
- select "Create new project"            
- select "Import Project"            
- select "Import from BitBucket Cloud"              
- select "Import from catarina_nogueira/devops-21-22-lmn-1211754"                 
- select "Set up CI/CD"       
- select "Configure pipeline"            

*Due to time constraints I was unable to explore a gitlab-ci.yml file in more depth.*                  

The first difference I found between GitLab and Jenkins it's the language used to set up a pipeline script:          
GitLab uses yaml while Jenkins uses Groovy.                 

Following this logic we should be able to adapt the Jenkinsfile written in Groovy from ca5/part2 to yaml.       
Although GitLab has a specific job order, we should be able to differentiate each stage into its            
specific stages(build,test or deploy);         
       






