# CA5/Part2 - Jenkins Practice               

## Creating a Pipeline with a Docker Container             

### By Catarina Nogueira                    

*Commands are run using **Git Bash Here** or **Windows Power-Shell**           
except if another tool is mentioned*               

**Step1:** Create part2 folder in CA5 folder             
- cd /c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA5             
- mkdir part2                      

**Step2:** Create readme file           
- cd /c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA5/part2               
- touch readme.md           

**Step3:** Setting a pipeline script via Ide                              
*using intelliJ*          
- create a Jenkinsfile in CA5/part2            
- see Jenkinsfile in gradle_basic_demo                                  

**Step4:** Run Jenkins            
- java -jar jenkins.war --httpPort=9090        

**Step5:** Installing the Javadoc publish html plugin                        
*jenkins should be running on http://localhost:9090/*                    
- login to jenkins                
- select dashboard               
- select manage jenkins                 
- select manage plugins                
- select available                
- type html publisher                                   
- install plugin                     
 
**Step6:** Installing the docker pipeline plugin                  
*jenkins should be running on http://localhost:9090/*                
- login to jenkins                
- select dashboard                 
- select manage jenkins               
- select manage plugins               
- select available             
- type docker pipeline                     
- install plugin                     

**Step7:** Create a Dockerfile                             
- see Dockerfile in gradle_basic_demo                   

**Step8:** Adding docker hub credentials in Jenkins                                     
*jenkins should be running on http://localhost:9090/*                          
- select dashboard            
- select manage jenkins              
- select manage credentials             
- global           
- add credentials                    
- set docker hub username               
- set docker hub password              
- set id; example: "dockerhub_credentials"              

**Step9:** Creating new pipeline job                
*jenkins should be running on http://localhost:9090/*                            
- select *new item*             
- select *pipeline*               
- name the job (I have named it as ca5part2)                         
- choose the option *Pipeline script from SCM*                       
- choose Git as SCM                           
- place repository URL https://bitbucket.org/catarina_nogueira/devops-21-22-lmn-1211754                              
- specify branch as */main                     
- specify script path to CA5/part2/gradle_basic_demo/Jenkinsfile                   

**Step10:** Had to change the .gitignore file in order for the gradle-wrapper.jar and build folder to be pushed to the remote repository                                                        
*I also forced the commit*              
- git add -f gradle-wrapper.jar      
- git commit -m "committing gradle-wrapper.jar"                   
- git push             
- git add -f build               
- git commit -m "committing build folder"         
- git push              

**Step11:**          
- on http://localhost:9090/       
- choose job ca5part2   
- choose build now          
- working as intended 

**Step12:** Committing updated readme                      
- git add .                         
- git commit -m "committing updated readme resolves #43"                      
- git push                             
- git tag ca5-part2                          
- git push origin ca5-part2                                  


