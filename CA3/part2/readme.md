# CA3/Part2 - Virtualization Practice                               

## Using Vagrant with Virtual Box                                                                   

### By Catarina Nogueira                                                            

*Commands are run using **Git Bash Here** or *Windows Power-Shell* except if another tool is mentioned*                               

## Tutorial for the online class                                                                        
**Step1:** Use **Git Bash Here** in folder                                                                                         
/c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754                                                        

**Step2:** Create part2 folder in CA3 folder                                  
- cd /c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA3                                         
- mkdir part2                                      

**Step3:** Create readme file                                            
- cd /c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA3/part2                                     
- touch readme.md                                   

**Step4:** Install Vagrant                                 
- follow the installation guide                                

**Step5:** To verify Vagrant installation                                
- open *Windows Power-Shell*                                 
- vagrant - v (version 2.2.19 - worked as intended)                                     

**Step6:** Create a folder where we want to initialize a vagrant project                                
- mkdir vagrant-project-1                                
- cd vagrant-project-1                                

**Step7:** Create a new Vagrant configuration file                          
- vagrant init envimation/ubuntu-xenial                             

**Step8:** Start the VM                               
- vagrant up                              

**Step9:** Start an SSH session to the VM:                             
- vagrant ssh                      
- vagrant halt (to shutdown the VM)                             

**Step10:** To edit the Vagrant file                               
- using **Git Bash Here** in folder vagrant-project-1             
- nano Vagrantfile                                      

**Step11:** Install Apache (provision section)                                   
- uncomment lines from the Vagrantfile from "config.vm-provision" onwards until "SHELL"                                                                   

**Step12:** Forward a port from the VM to the host machine                                   
- uncomment lines from the Vagrantfile from "config.vm.network" "forwarded-port"                                 
- change "host:8080" to "host:8010"                                        

**Step13:** Setup a static private IP on the VM                             
- uncomment lines from the Vagrantfile from "config.vm.network" "private_network"                                 
- change "ip: 192.168.33.10" to "ip: 192.168.56.5"                                    

**Step14:** Set up a shared folder for the webserver pages                               
- uncomment lines from the Vagrantfile from "config.vm.synced_folder"                        
- change "../data", "/vagrant_data" to "./html", "/var/www/html"                         

**Step15:** Save changes to Vagrantfile                        
-ctrl + X plus Y                               

**Step16:** After editing the Vagrantfile provision                              
- vagrant reload --provision                                

**Step17:** Removed the box from my host machine                     
- vagrant box remove envimation/ubuntu-xenial                                  

## Tutorial for the part2 assignment                                                      

### Point 1                                
**Step18:** Clone "vagrant-multi-spring-tut-demo" repository to dir CA3/part2 dir                              
- cd /c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA3/part2                          
- git clone https://Catarina_Nogueira@bitbucket.org/atb/vagrant-multi-spring-tut-demo.git                                           

**Step19:** Copying the application "react-and-spring-data-rest-basic" into CA3/part2 folder                    
- cp /c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA2/part2/react-and-spring-data-rest-basic  /c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA3/part2                         

### Point 2 and 4                                                          
**Step20:** Open Vagrant file in IntelliJ to use my own gradle version of the spring application                                                  
- change line 76 to git clone https://Catarina_Nogueira@bitbucket.org/catarina_nogueira/devops-21-22-lmn-1211754.git                       
- change line 77 to cd /c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA3/part2/react-and-spring-data-rest-basic                                                
- change line 14 to sudo apt-get install openjdk-11-jdk-headless -y                      
- changed the ubuntu version (see line 6, 23, 52) to "ubuntu/bionic64"                          
- added line 82, adapting the path to my own application                     

*The professor's Vagrant file is configured to JDK 8 for his spring application; however, we need to change the
Vagrant file to install JDK 11 since our own spring application requires this version. 
This also meant changing the ubuntu version from **envimation/ubuntu-xenial** 
to **ubuntu/bionic64** since this ubuntu version supports JDK 11.*                     

### Point 3                                                     
**Step21:** Changed the dir of the new edited Vagrant file                  
- from ..CA3/part2/vagrant-multi-spring-tut-demo to ..CA3/part2                                

**Step22:** Start the newly created vagrant configuration file            
- using **Git Bash Here**                           
- /c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA3/part2
- vagrant up (both VM's running as intended)                         

*Since it will be necessary to alter the application from CA2/part2, I have opted to make a copy of this previous altered application
into the CA3/part2 folder; should this application be used in the future on other assignments.*                                                                        

**Step23:** Committing changes to repository                                       
- git add .                        
- git commit -m "committing changes to repository (resolves #33, #34, #35, #36)"                                   
- git push                             

### Point 5 and 6                                                   
**Step24:** Making changes to my "react-and-sprint-data-rest-basic" application                      

*Used the professor repository to make changes to my application: git clone https://Catarina_Nogueira@bitbucket.org/atb/tut-basic-gradle.git
Verified his commits and made the necessary changes to my application.*                                                                                    
                                                                     
- in build.gradle file, see line 30, edited:                    
  - from assembleScript = "run build"                                
  - to assembleScript = 'run webpack'                                                                                                           
  
                                                                 
- in package.json file, see line 28:                                    
  - added "watch": "webpack --watch -d"                                                           
  
                                                 
- in build.gradle file:                              
  - added id 'war' (see line 6)               
  - providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat' (see line 25)                                  
  
                                                      
- added java.Class ServletInitializer and made the necessary changes in Class DatabaseLoader and Employee                             

                                                     
- in application.properties file added:           
  - server.servlet.context-path=/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT             
    spring.data.rest.base-path=/api                              
    spring.datasource.url=jdbc:h2:tcp://192.168.56.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE *(changed IP to "192.168.56.11" since it is the one defined in the Vagrant file)*                                              
    spring.datasource.driverClassName=org.h2.Driver                    
    spring.datasource.username=sa                         
    spring.datasource.password=                  
    spring.jpa.database-platform=org.hibernate.dialect.H2Dialect                 
    spring.jpa.hibernate.ddl-auto=update                       
    spring.h2.console.enabled=true                                
    spring.h2.console.path=/h2-console                       
    spring.h2.console.settings.web-allow-others=true                                     
  
                                                                          
- in app.js file, see line 18, edited:                          
  - from '/basic-0.0.1-SNAPSHOT..                 
  - to '/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT..                                                
  
                                                
- in index.html file, see line 6, edited:             
  - from <link rel="stylesheet" href="/main.css" />                 
  - to <link rel="stylesheet" href="main.css" />                                        
  
                                          
- in .gitignore file in app, copied the complete content from the professors repository                              

                                                 
**Step25:** Committing changes to repository                       
- git add .               
- git commit -m "updating readme and committing application (resolves #37)"                  
- git push       

**Step26:** Starting the VM's      
- using **Git Bash Here** in folder ..CA3/part2                                 
- vagrant up                                
- vagrant reload --provision (to run recent changes to vagrant file)     
- vagrant halt (to stop the VM's to correct errors in the build)

**Step27:** Editing various files due to errors with the application build            
- edited java.Class DataBaseLoader                
- edited Vagrant file                     
- edited build.gradle          
- edited app.js file                        
                                                   
- using **Git Bash Here** in folder ..CA3/part2
- vagrant up (running the VM again)              

*Due to java build fails, had to remove the VM's, commit the changes in the application 
to the repository and, run Vagrant up various times until I had a stable application.                                    
These issues were unrelated to the vagrant file provisioning but with the application itself.                            
Eventually, I was able to run the application as I intended.*                                

**Step28:** Testing the VM db and Web                                 
- run URL http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console (db VM)  
- For the connection string use: jdbc:h2:tcp://192.168.56.11:9092/./jpadb                               
![](annex1.PNG)

- run URL http://192.168.56.10:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/ (web VM)                           
![](annex2.PNG)

**Step29:** Committing files to repository                                  
- git add .                      
- git commit -m "updating readme"                                 
- git push                        
- git tag ca3-part2                       
- git push origin ca3-part2                    
          
            