# CA3/Part2 - Virtualization Practice             

## Using Vagrant with VMware                 

### By Catarina Nogueira

*Commands are run using **Git Bash Here** or *Windows Power Shell* except if another tool is mentioned*                    

## 1. Introduction to Virtualization

In computing, virtualization is the act of creating a virtual 
(rather than actual) version of something, including virtual computer hardware 
platforms, storage devices, and computer network resources.
                               
Software executed on these virtual machines is separated from the underlying hardware resources.              
For example, a computer that is running Arch Linux may host a virtual machine that looks like a computer with 
the Microsoft Windows operating system; Windows-based software can be run on the virtual machine.                     
In hardware virtualization, the host machine is the machine that is used by the virtualization and the 
guest machine is the virtual machine.              
                           
The words host and guest are used to distinguish the software that runs on the physical machine from the 
software that runs on the virtual machine.
The software that creates a virtual machine on the host hardware is called a hypervisor.           
A hypervisor is a thin layer of software that interacts with the underlying resources of a physical computer
(called the host) and allocates those resources to other operating systems (known as guests).
The guest OS requests resources from the hypervisor.


## 1.1 VirtualBox
             
VirtualBox is a powerful virtualization product for enterprise as well as home use.               
Not only is VirtualBox an extremely feature rich, high performance product for enterprise customers, 
it is also the only professional solution that is freely available as Open Source Software.              

Users of VirtualBox can load multiple guest OSes under a single host operating-system (host OS). 
Each guest can be started, paused and stopped independently within its own virtual machine (VM). 
The user can independently configure each VM.                           
The host OS and guest OSs and applications can communicate with each other through 
a number of mechanisms including a common clipboard and a virtualized network facility.                  
Guest VMs can also directly communicate with each other if configured to do so.                          
 

## 1.2 VMware

VMware is a virtualization and cloud computing software vendor.
VMware products include virtualization, networking and security management tools, 
software-defined data center software, and storage software.
VMware offers applications for desktops as well as servers.                     

The hypervisor separates each guest OS so each can run without interference from the others.                    
Should one guest OS suffer an application crash, become unstable, or become infected with malware, 
it won't affect the performance or operation of other operating systems running on the host.                 
                  
VMware Workstation (Desktop Virtualization) includes Type 2 hypervisors. 
Unlike a Type 1 hypervisor, which replaces the underlying OS altogether, 
a Type 2 hypervisor runs as an application on the desktop OS and lets desktop users run a second 
OS atop their main (host) OS.

## 1.3 VirtualBox vs VMware

| VirtualBox                                                                                                                                                                                                                                                  | VMware | 
|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Provides virtualization for both hardware and software                                                                                                                                                                                                      | Provides virtualization only for hardware.
| VirtualBox is available for Linux, Windows, Solaris, macOS, FreeBSD etc. There exists a wide scope of supporting various OS.                                                                                                                                | VMare is available for Linux and Windows. You must install VMware Fusion/Fusion Pro if you have a Mac. There is a narrow scope of supporting various OS.
| On VirtualBox-based VMs, the following guest operating systems can be installed: Linux, Windows, Solaris, FreeBSD, and macOS.                                                                                                                               | VMware also supports operating systems such as Windows, Linux, Solaris, and Mac. The only distinction is that to operate macOS virtual machines, VMware requires VMware Fusion/Fusion Pro.
| VirtualBox supports  VDI (Virtual Disk Image),  Virtual Machine Disk (VMDK),   Virtual Hard Disk (VHD).                                                                                                                                                     | VMware support Virtual Machine (VMDK)
| virtualBox provides a user-friendly interface.                                                                                                                                                                                                              | VMware provides a complicated user interface.
| It can only support 3D graphics Up to OpenGL 3.0 and Direct3D 9. 		                                                                                                                                                                                      | VMware provides 3D graphics with DirectX 10 and OpenGL 3.3 support for all of its products.
| Video memory, in case of VirtualBox, is limited to 128 MB.                                                                                                                                                                                                   | Video memory, incase of VMware, is limited to 2 GB.
| To support 3D graphics in VirtualBox, you must manually enable 3D acceleration on the virtual machine.                                                                                                                                                      | 3D acceleration is enabled by default.
| VirtualBox supports snapshots, which means it allows you to save and restore the state of a virtual machine.                                                                                                                                                | Snapshots only supported on paid virtualization products, not on VMware Workstation Player
| The following network modes are available in VirtualBox:Not attached, Network Address Translation (NAT), NAT Network, Bridged networking, Internal networking, Host-only networking, Generic networking, UDP Tunnel, Virtual Distributed Ethernet (VDE)     | VMware supports the following network nodes:Network Address Translation (NAT), Host-only networking, Virtual network editor (on VMware workstation and Fusion Pro)
| It is an open source tool.                                                                                                                                                                                                                                  | It is not an open source tool.
| Shared folders are available in Oracle VirtualBox.                                                                                                                                                                                                          | Shared folders are available in VMware products such as VMware Workstation, VMware Player, and VMware Fusion. Virtual machines that run on an ESXi host are not supported, and shared files must be manually built.
| Linked clones are supported by VirtualBox.                                                                                                                                                                                                                  | Linked clones are supported by VMware Workstation, VMware Fusion Pro. As for VMware ESXi, you can create linked clones manually by writing special scripts for PowerCLI.
| VirtualBox is a Type 2 Hypervisor.                                                                                                                                                                                                                          | VMware ESXi is a Type 1 Hypervisor that works directly on the host machine’s hardware resources. Some VMware products, such as VMware Player, VMware Workstation, and VMware Fusion, are Type 2 Hypervisors.

Each product has its strengths and relative weaknesses in different areas.                          
VMware's products offer better support for 3D graphics, while VirtualBox supports more virtual disk images,
which are files that contain virtual machine data.                           

The most obvious distinction between the two is that VirtualBox is a free and open-source software programme
whereas VMware is solely available for personal use.                              
The free version of VMware for personal and educational use offers limited functionality.                     
Clones and snapshots, for example, are not supported.                            


## 1.4 Vagrant

Vagrant is a tool for building and managing virtual machine environments in a single workflow.                  
With a focus on automation, Vagrant lowers development environment setup time, 
increases production parity, and makes the "works on my machine" excuse a relic of the past.                      
Vagrant is designed for everyone as the easiest and fastest way to create a virtualized environment.                

By default, VirtualBox is the default provider for Vagrant. 
VirtualBox is still the most accessible platform to use Vagrant: 
it is free, cross-platform, and has been supported by Vagrant for years. 

Vagrant allows specifying the default provider to use by setting the VAGRANT_DEFAULT_PROVIDER 
environmental variable. Just set VAGRANT_DEFAULT_PROVIDER to the provider you wish to be the default.                   
For example, if you use Vagrant with VMware, you can set the environmental variable to vmware_desktop 
and it will be your default.                 
                         
HashiCorp develops an official VMware Workstation provider for Vagrant.                       
This provider allows Vagrant to power VMware based machines and take advantage of the improved
stability and performance that VMware software offers.              
This provider is a drop-in replacement for VirtualBox. However, there are some VMware-specific things such
as box formats, configurations, etc.                       

## 1.5 Vagrant: Multi - Machines

Vagrant is able to define and control multiple guest machines per Vagrantfile. 
This is known as a "multi-machine" environment.

These machines are generally able to work together or are somehow associated with each other. 
Here are some use-cases people are using multi-machine environments for today:
- Accurately modeling a multi-server production topology, such as separating a web and database server.               
- Modeling a distributed system and how they interact with each other.                     
- Testing an interface, such as an API to a service component.                            
- Disaster-case testing: machines dying, network partitions, slow networks, inconsistent world views, etc.                         

Historically, running complex environments such as these was done by flattening them onto a single machine. 
The problem with that is that it is an inaccurate model of the production setup, which can behave far differently.
Using the multi-machine feature of Vagrant, these environments can be modeled in the context of a single 
Vagrant environment without losing any of the benefits of Vagrant.                          

Multiple machines are defined within the same project Vagrantfile using the config.vm.define method call. 
It creates a Vagrant configuration within a configuration.                   

When using these scopes, order of execution for things such as provisioners becomes important. 
Vagrant enforces ordering outside-in, in the order listed in the Vagrantfile.                 

The moment more than one machine is defined within a Vagrantfile, 
the usage of the various vagrant commands changes slightly.              
Commands that only make sense to target a single machine, such as vagrant ssh, 
now require the name of the machine to control.                   
Using the example above, you would say vagrant ssh web or vagrant ssh db.                 

Other commands, such as vagrant up, operate on every machine by default. 
So if you ran vagrant up, Vagrant would bring up both the web and DB machine.                    

You can also specify a primary machine. The primary machine will be the default machine used when a 
specific machine in a multi-machine environment is not specified.                    
                            
*I have obtained most of my information from https://www.vagrantup.com/docs                           
The vagrant site is very complete, sharing information on the compatible hypervisors as well the necessary
configuration steps with each hypervisor.               
I have found this website helpful and informative, specially for the implementation of this assignment.*            

## 2. Alternative implementation                   

**Step1:** Use **Git Bash Here** in folder                                                                                                                
/c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754                                

**Step2:** Create part2-Alternative folder in CA3 folder                             
- cd /c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA3                                    
- mkdir part2                                   

**Step3:** Create readme file                                            
- cd /c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA3/part2-Alternative                       
- touch readme.md                                          

**Step4:** Install VMware for Windows                                                    
- https://customerconnect.vmware.com/en/downloads/details?downloadGroup=WKST-PLAYER-1623-NEW&productId=1039&rPId=85399                                                           

**Step5:** Re-use the previous files to the current dir CA3/part2-Alternative                                         
- copied the Vagrant file from CA3/part2 into CA3/part2-Alternative                                        
- copied the react-and-sprint-data-rest-basic application from CA3/part2 into CA3/part2-Alternative                                                

**Step6:** Had to change the vagrant box version since it was not compatible with VMware.                               
used the following one:                                     
- config.vm.box = "hashicorp/bionic64"                                       
- see https://app.vagrantup.com/hashicorp/boxes/bionic64                    
- see line 9, 24 and 50 in the Vagrant file                           
                
**Step7:** Change the VM provider to the VMware Workstation:                                   
- config.vm.provider "vmware_desktop" do |v|                    
- see line 55 in the Vagrant file                                 

**Step8:** updating readme                                                  
- git add .                                                 
- git commit -m "updating vagrant file"                                                           
- git push                                    

**Step9:** Installation of the Vagrant VMware provider:                                                 
- installing Vagrant VMware Utility (see https://www.vagrantup.com/vmware/downloads)                                                
- vagrant plugin install vagrant-vmware-desktop (run using **Git Bash Here**)                                                  

**Step10:** Running Vagrant file                         
- vagrant up (using **Git Bash Here** in ../devops-21-22-lmn-1211754/CA3/part2-Alternative folder)                                       

*Because I haven't changed the private network from VirtualBox
in the Vagrant file I was unable to run the VM's with VMware since further edits were necessary.                   
After some research, I verified that upon VMware installation, VMware defined an automatic **VMware Network Adapter
VMnet1** with an IP: 192.168.42.1 with a network mask 255.255.255.0.
Since usually, and IP ending in 1 is set to a Router, 
I defined the IPs for the VM's to 192.168.42.11 and 192.168.42.10*                  

**Step11:** Editing Vagrant file:                             
- see line 26, from "192.168.56.11" to "192.168.42.11" (db)                                                              
- see line 52, from "192.168.56.11" to "192.168.42.10" (web)                       
- see line 74, changed the dir path to devops-21-22-lmn-1211754/CA3/part2-Alternative..                                           

**Step12:** Editing application.properties file:                                                
- see line 3:                                                            
  - from spring.datasource.url=jdbc:h2:tcp://192.168.56.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE                                                 
  - to spring.datasource.url=jdbc:h2:tcp://192.168.42.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE                       

**Step13:** Starting the VM's                       
- vagrant up using **Git Bash Here** in ..CA3/part2-Alternative                                  

**Step14:** Testing the VM db and Web                             
- run URL http://192.168.42.10:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console (db VM)                                  
- For the connection string use: jdbc:h2:tcp://192.168.42.11:9092/./jpadb                                       
  ![](annex1.PNG)                             

- run URL http://192.168.42.10:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/ (web VM)                                                
  ![](annex2.png)                         

**Step15:** Updating readme                                           
- git add .                                          
- git commit -m "updating completed readme"                                                     
- git push                       
- git tag ca3-part2Alternative                        
- git push origin ca3-part2Alternative                                            


## Conclusion                     

*Regarding this alternative implementation I don't have much to elaborate.              
Most of the issues I have faced regarding the provisioning of the VM's while using Vagrant were,             
for the most part, non-Vagrant related.                                                               
It was mostly due to issues with the application or missing files in the repository; these issues were detailed 
in the implementation section.                                                   
This happened with the implementation of the part2 and part2-Alternative.*                  

*In fact, there wasn't much difference between setting a Vagrant file using VirtualBox as an hypervisor or VMware.                  
Perhaps this was due to Vagrant's Doc library, with a complete install and configuration guide.                      
Vagrant's website provides information regarding all the compatible hypervisors with emphasis on installation, 
configuration, usage and common issues (to name a few).*                                          

*However, if I would have to make a choice between both hypervisors, with the knowledge I have retained so far, 
I would keep using **VirtualBox** for the following reasons:*                    
- *it's free and open source;*                                 
- *it's the default provider for Vagrant;*                                             
- *it's GUI seems more user-friendly than VMware (I don't consider this last point too relevant since one can provision 
the VM only using the command line);*                                                                                                     




                   
