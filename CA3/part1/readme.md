# CA3/Part1 - Virtualization Practice                                 

## Using Virtual Box                                 

### By Catarina Nogueira                             

*Commands are run using **Git Bash Here** or *Virtual Machine* except if another tool is mentioned*                            

## Point 1                        
**Step1:** Use **Git Bash Here** in folder                                       
/c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754                             

**Step2:** Create CA3 folder                               
- mkdir CA3                         

**Step3:** Create part1 folder                            
- cd /c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA3                
- mkdir part1                     

**Step4:** Create readme file                   
- cd /c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA3/part1                   
- touch readme.md                      

**Step5:** Commit changes to repository                                                    
- git add .                             
- git commit -m "Create CA3, part1 folder and readme.md. resolves #28, #29"                                                        
- git push                                       

**Step6:** Install Virtual Box                           
- go to https://www.virtualbox.org/wiki/Downloads                        
- choose VirtualBox 6.1.34 platform package Windows Hosts (for Windows users)                     
- follow the steps in the installation guide                             

**Step7:** Create new Virtual Machine (step by step)                          
- open Oracle VM Virtual Box             
- select "New" on the Oracle VM Virtual Box Manager                 
- select a name for your Virtual Machine (mine: ubuntu-devops-CA3)                   
- select memory (RAM) size (2048 MB)                   
- select the option "Create a virtual hard disk now"                 
- select the option "VDI (VirtualBox Disk Image)"                
- select the option "Dynamically allocated"                  
- select "Create"                  

**Step8:** Edit VM settings                                       
- select your VM                                          
- select "Settings"                                  
- select "Storage"                                  
- select "Empty"                            
- select "Optical Drive" disk icon                                     
  - select mini iso file                                     
  - to download the mini iso file go here: https://help.ubuntu.com/community/Installation/MinimalCD#A64-bit_PC_.28amd64.2C_x86_64.29_.28Recommended.29                                            
  - download the Ubuntu 18.04 "Bionic Beaver" 64MB version                                 
- check the "Live CD/DVD" box                                    

**Step9:** Install Ubuntu 18.04                                     
- select your VM                             
- select start                               
- we will now Install Ubuntu                               
- follow the configuration steps                                       
- when the installation finishes                                               
- select "Settings"                        
- select "Storage"                                   
- remove mini iso file from the "Optical Drive" disk icon                               
- select "Remove Disk from Virtual Device"                                        
- start VM                                    

**Step10:** Create a host-only network (your VM should be powered-off)                              
- go to main menu                               
- select "File"                               
- select "Host Network Manager"                                          
- already verified that a Host-only adapter already, so I didn't create a new one                                   
- now go to your VM                              
- select "Settings"                           
- select "Network"                               
- select "Adapter 2"                             
- check the box "Enable Network Adapter"                             
- in the box "Attached to" change to "Host-only Adapter"                            
- select the newly created adapter ("VirtualBox Host-Only Ethernet Adapter")                                 

**Step11:** Continue the VM setup                                          
- start VM                                  
- *the following commands were run in the VM*                              
- sudo apt update                      
- sudo apt install net-tools                           

**Step12:** Continue the VM setup    
*the following commands were run in the VM*               
- Edit the network configuration file              
  - sudo nano /etc/netplan/01-netcfg.yaml                      
    - adding the following contents to the file:                       
    - network:                 
        version: 2              
        renderer: networkd                       
        ethernets:                   
          enp0s3:                         
            dhcp4: yes                   
          enp0s8:                              
            addresses:                        
              - 192.168.56.5/24                                              
  - ctrl+x plus S (to exit nano and save changes)                                      

**Step13:** Continue the VM setup   
*the following commands were run in the VM*                    
- Apply the previous changes                           
  - sudo netplan apply                                 

**Step14:** Continue the VM setup    
*the following commands were run in the VM*           
- Install openssh-server                                       
  - sudo apt install openssh-server                                           

**Step15:** Continue the VM setup        
*the following commands were run in the VM*            
-Enable password authentication for ssh                                   
  - sudo nano /etc/ssh/sshd_configS                                  
  - uncomment line "PasswordAuthentication yes"                              
  - ctrl+x plus S (to exit nano and save changes)                                    
  - sudo service ssh restart                                      

**Step16:** Continue the VM setup   
*the following commands were run in the VM*                 
- Install an ftp server                            
  - sudo apt install vsftpd                                  

**Step17:** Continue the VM setup    
*the following commands were run in the VM*                
- Enable write access for vsftpd                                       
  - sudo nano /etc/vsftpd.conf                                         
  - uncomment line "write_enable=YES"                                 
  - ctrl+x plus S (to exit nano and save changes)                                  
  - sudo service vsftpd restart                                     

**Step18:** Use SSH to connect to the VM                                       
- open **Windows Power-Shell**                             
- type your VM user-name followed by the IP you configured in step **12**                                                           
- ssh catarina@192.168.56.5                                                                                        

**Step19:** Install Git   
*the following commands were run via SSH (**Windows Power-Shell**) connected to the VM*                            
- sudo apt install git                                

**Step20:** Install JDK 8     
*the following commands were run via SSH (**Windows Power-Shell**) connected to the VM*                         
- sudo apt install openjdk-8-jdk-headless                                      

## Point 2                           
**Step21:** Clone the repository to your VM      
*the following commands were run via SSH (**Windows Power-Shell**) connected to the VM*                 
- git clone https://Catarina_Nogueira@bitbucket.org/catarina_nogueira/devops-21-22-lmn-1211754.git                           

## Point 3 and 4
**Step22:** Run the CA1 application  
*the following commands were run via SSH (**Windows Power-Shell**) connected to the VM*                                  
- chmod u+x mvnw (first I need to grant permission)                                             
- cd devops-21-22-lmn-1211754                              
- cd CA1/                    
- cd tut-react-and-spring-data-rest/                    
- cd basic/                         
- ./mvnw install               
- ./mvnw spring-boot:run                              
- run the web application from the host machine browser http://192.168.56.5:8080/ (works as intended)                                  

## Point 3 and 5                        
**Step23:** Run the CA2/part1 application                               
*the following commands were run via SSH (**Windows Power-Shell**) connected to the VM*                             
- chmod u+x gradlew (first I need to grant permission)                                       
- cd CA2/                    
- cd part1/                      
- cd gradle_basic_demo/                                
- ./gradlew build                       
- ./gradlew runServer                                               

*The following changes were made outside the VM in the repository on the host machine, **IDE IntelliJ***                                        
- in CA2/part1 build.gradle file, task runClient (line 59) changed from 'localhost' to '192.168.56.5'                            
- using *Git Bash Here* (directory gradle_basic_demo/) run command ./gradlew runClient (worked as intended)                                                                  

*To run CA2/part1 application the VM can only run the server, since it doesn't have a graphical interface. Due to this                       
we need to run the client on the host machine*                                      

## Point 3 and 4                         
**Step23:** Run the CA2/part2 application    
*the following commands were run via SSH (**Windows Power-Shell**) connected to the VM*             
- chmod u+x gradlew (first I need to grant permission)                         
- cd CA2/                                
- cd part2/                                 
- cd react-and-spring-data-rest-basic/                                              
- the application had compatibility issues because I installed JDK 8 (see step 20) and JDK11 was required                                    
- sudo apt install openjdk-11-jdk-headless (installed JDK 11)                                         
- ./gradlew build                        
- ./gradlew bootRun                                         
- run the web application from the host machine browser http://192.168.56.5:8080/ (works as intended)                                       

**Step24:** Completed readme tutorial, committing changes to the repository, adding ca3-part1 tag                                
- git add .                    
- git commit -m "committing completed readme"                                 
- git push                        
- git tag ca3-part1                                 
- git push origin ca3-part1                                       








