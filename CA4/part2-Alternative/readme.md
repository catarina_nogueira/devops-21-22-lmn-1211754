# CA4/Part2-Alternative - Kubernetes

## Option 1 : Exploring Kubernetes

### By Catarina Nogueira  


# 1.Introduction

*In order to understand Kubernetes and its features, I had to understand the following concepts or tools:
- **Containers**
- **Docker** 
- **Platform as a Service(PaaS)**
- **Volumes**
The following points will be an overview of these concepts.*


## Containers

Containers allow creating isolated environments for your applications while sharing
a single operating system.

Containers are significantly more lightweight than VMs (occupy less space; are easier
to create and use), while still offering a good level of isolation and other benefits of
VMs.

Containers are similar to VMs, but they have relaxed isolation properties to share the 
Operating System (OS) among the applications. Therefore, containers are considered lightweight. 
Similar to a VM, a container has its own filesystem, share of CPU, memory, process space, and more. 
As they are decoupled from the underlying infrastructure, they are portable across clouds and 
OS distributions.

An image is a read-only template with instructions for creating a container. Often, an image is
based on another image, with some additional customization.

A container image represents binary data that encapsulates an application and all its software 
dependencies. Container images are executable software bundles that can run standalone and that 
make very well-defined assumptions about their runtime environment.


## Docker

Docker is a set of platform as a service (PaaS) products that use OS-level virtualization to 
deliver software in packages called containers.

Docker can package an application and its dependencies in a virtual container that can run on any Linux, 
Windows, or macOS computer. This enables the application to run in a variety of locations, 
such as on-premises, in public or private cloud. 
When running on Linux, Docker uses the resource isolation features of the Linux kernel.

Software: 
The Docker daemon, called dockerd, is a persistent process that manages Docker containers and 
handles container objects. The daemon listens for requests sent via the Docker Engine API. 
The Docker client program, called docker, provides a command-line interface (CLI), that allows users to 
interact with Docker daemons.

Objects: 
Docker objects are various entities used to assemble an application in Docker. 
The main classes of Docker objects are images, containers, and services.
- A Docker container is a standardized, encapsulated environment that runs applications. 
A container is managed using the Docker API or CLI.

- A Docker image is a read-only template used to build containers. 
Images are used to store and ship applications.

- A Docker service allows containers to be scaled across multiple Docker daemons. The result is known 
as a swarm, a set of cooperating daemons that communicate through the Docker API.

Registries: 
A Docker registry is a repository for Docker images. Docker clients connect to registries to download 
("pull") images for use or upload ("push") images that they have built. Registries can be public or 
private. Two main public registries are Docker Hub and Docker Cloud. Docker Hub is the default registry 
where Docker looks for images. Docker registries also allow the creation of notifications based on events.

Docker Compose is a tool for defining and running multi-container Docker applications.
It uses YAML files to configure the application's services and performs the creation and start-up 
process of all the containers with a single command. The docker-compose CLI utility allows users to run 
commands on multiple containers at once, for example, building images, scaling containers, running 
containers that were stopped, and more.


## Platform as a Service

Platform as a service (PaaS) or application platform as a service (aPaaS) or platform-based service is 
a category of cloud computing services that allows customers to provision, instantiate, run, and manage 
a modular bundle comprising a computing platform and one or more applications, without the complexity 
of building and maintaining the infrastructure typically associated with developing and launching the 
application(s); and to allow developers to create, develop, and package such software bundles.

PaaS can be delivered in three ways:
- As a public cloud service from a provider, where the consumer controls software deployment with 
minimal configuration options, and the provider provides the networks, servers, storage, operating 
system (OS), middleware (e.g. Java runtime, .NET runtime, integration, etc.), database and other 
services to host the consumer's application.
- As a private service (software or appliance) behind a firewall.
- As software deployed on public infrastructure as a service.


## Volumes
On-disk files in a container are ephemeral, which presents some problems for non-trivial 
applications when running in containers. One problem is the loss of files when a container crashes.
Volumes are the preferred mechanism for persisting data generated by and used by containers.
Volume drivers let you store volumes on remote hosts or cloud providers, to encrypt the contents 
of volumes, or to add other functionality.
At its core, a volume is a directory, possibly with some data in it, which is accessible to the 
containers. 
How that directory comes to be is determined in the container image.


# 2.Kubernetes

Kubernetes, also known as K8s, is an open-source system for automating deployment, 
scaling, and management of containerized applications. It groups containers that make up an application 
into logical units for easy management and discovery.
It is specially designed to manage container platforms like Docker, containerd, etc.

Kubernetes is a portable, extensible, open source platform for managing containerized 
workloads and services, that facilitates both declarative configuration and automation.

Containers are a good way to bundle and run your applications. In a production environment, 
you need to manage the containers that run the applications and ensure that there is no downtime.
Kubernetes provides you with a framework to run distributed systems resiliently. It takes care of 
scaling and failover for your application, provides deployment 
patterns, and more.

Kubernetes is not a traditional, all-inclusive PaaS (Platform as a Service) system. Since Kubernetes 
operates at the container level rather than at the hardware level, it provides some generally 
applicable features common to PaaS offerings, such as deployment, scaling, load balancing, and lets 
users integrate their logging, monitoring, and alerting solutions.
However, Kubernetes is not monolithic, and these default solutions are optional and pluggable. 
Kubernetes provides the building blocks for building developer platforms, but preserves user choice 
and flexibility.

When you deploy Kubernetes, you get a cluster.

A Kubernetes cluster consists of a set of worker machines, called nodes, that run containerized 
applications. Every cluster has at least one worker node.

Kubernetes runs your workload by placing containers into Pods to run on Nodes. A node may be a virtual 
or physical machine, depending on the cluster.

A kubelet is an agent that runs on each node in the cluster. It makes sure that containers are 
running in a Pod.

The container runtime is the software that is responsible for running containers.

While Kubernetes supports more container runtimes than just Docker, Docker is the most commonly
known runtime, and it helps to describe Pods using some terminology from Docker.

In terms of Docker concepts, a Pod is similar to a group of Docker containers with shared namespaces
and shared filesystem volumes.

Pods are the smallest deployable units of computing that you can create and manage in Kubernetes.

A Pod (as in a pod of whales or pea pod) is a group of one or more containers, with shared storage and 
network resources, and a specification for how to run the containers. A Pod's contents are always co-located and 
co-scheduled, and run in a shared context. A Pod models an application-specific "logical host": 
it contains one or more application containers which are relatively tightly coupled. In non-cloud contexts, 
applications executed on the same physical or virtual machine are analogous to cloud applications 
executed on the same logical host.

As well as application containers, a Pod can contain init containers that run during Pod startup.

Pods in a Kubernetes cluster are used in two main ways:

Pods that run a single container. The "one-container-per-Pod" model is the most common Kubernetes 
use case; in this case, you can think of a Pod as a wrapper around a single container; Kubernetes 
manages Pods rather than managing the containers directly.

Pods that run multiple containers that need to work together. A Pod can encapsulate an application 
composed of multiple co-located containers that are tightly coupled and need to share resources. 
These co-located containers form a single cohesive unit of service—for example, one container 
serving data stored in a shared volume to the public, while a separate sidecar container refreshes 
or updates those files. The Pod wraps these containers, storage resources, and an ephemeral network 
identity together as a single unit.

When a Pod gets created (directly by you, or indirectly by a controller), the new Pod is scheduled 
to run on a Node in your cluster. The Pod remains on that node until the Pod finishes execution, 
the Pod object is deleted, the Pod is evicted for lack of resources, or the node fails.

Controllers for workload resources create Pods from a pod template and manage those Pods on your 
behalf.

Also, a Pod is not a process, but an environment for running container(s). 
A Pod persists until it is deleted.

Kubernetes supports many types of volumes. A Pod can use any number of volume types simultaneously. 
Ephemeral volume types have a lifetime of a pod, but persistent volumes exist beyond the lifetime 
of a pod. When a pod ceases to exist, Kubernetes destroys ephemeral volumes; however, 
Kubernetes does not destroy persistent volumes. For any kind of volume in a given pod, data is 
preserved across container restarts.


# 3.Docker vs Kubernetes

| Docker                                                            | Kubernetes | 
|-------------------------------------------------------------------|--------------------------------------------------------------|
| Easier installation                                               | Complex installation
| Lightweight and easier to learn but limited functionality         | More complex with a steep learning curve, but more powerful
| Manual scaling                                                    | Supports auto-scaling
| Needs third party tools for monitoring                            | Built-in monitoring
| Auto load balancer                                                | Manual setup of load balancer
| Integrated with Docker CLI 		                                | Need for separate CLI tool

Simply put, Docker is a containerization platform, which means it is responsible for container
creation, whereas Kubernetes is a container orchestration platform (ex: manages multi-container
applications). 
Both tools are technologies with different scopes. One can use Docker without Kubernetes and vice versa. 
However, it seems that instead of a Docker vs Kubernetes approach they in fact should complement each 
other; **more than alternatives it seems they are complementary**.

To sum it up:
- Docker’s home turf is development: includes configuring, building, and distributing 
containers using Continuous-Integration and Continuous Delivery (CI/CD) and DockerHub as an image 
registry. 
- Kubernetes excels in operations, allowing to use existing Docker containers while tackling the complexities 
- of deployment, networking, scaling, and monitoring.


# 4.How to use Kubernetes to solve the same goals as Docker

*Since I have detailed a lot of information regarding Kubernetes and its components above, I'll be
more concise in this part of the report.*

### Point 1 and 2
As we know, Docker focus is on container creation and Kubernetes supports Docker containers

Instead of using a docker-compose.yaml file we should also use, as an example, a simple-pod.yaml.
As with docker we can set one container or multiple containers within the Pod.

However, in order to run the Pod, first we must configure and deploy a Cluster (detailed in the Kubernetes 
section above).
As explained previously, a Cluster consists of Nodes that hosts the Pods, which in turn will initialize
the container or containers (depending on the Pod container configuration).

### Point 3
Assigning names and tags to a container image is similar if not identical to Docker.
Typically a container image of an application is created and published before referring to it
in a Pod. In the same way, we can simply set a container image within the pod yaml file.
Actually when publishing an image via Kubernetes, if a registry hostname is not specified, 
Kubernetes assumes the DockerHub registry.

### Point 4
Kubernetes has more options on how to persist date since it supports more variety of types of volumes, 
in opposition to Docker.
Due to this, it will require more configuration than in a docker-compose file.


*Naturally, this report was heavily based and written from https://kubernetes.io/docs/home/ and
https://docs.docker.com/*



