# CA4/Part1B - Container Practice          

## Using DockerFile with DockerDesktop              

## Creating DockerFile to run server               

### By Catarina Nogueira                   

*Commands are run using **Git Bash Here** or **Windows Power-Shell** 
except if another tool is mentioned*            

**Step1:** Use **Git Bash Here** in CA4 folder                                                                                                                  
/c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA4                  

**Step2:** Create part1B folder in CA4 folder                   
- cd /c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA4            
- mkdir part1B               

**Step3:** Create readme file           
- cd /c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA4/part1B                
- touch readme.md                

**Step4:** Copy the gradle_basic_demo             
*using **Git Bash Here** in CA4/part1B folder                    
git clone https://Catarina_Nogueira@bitbucket.org/luisnogueira/gradle_basic_demo.git                    

**Step5:** Run the application gradle_basic_demo from                 
*using **Git Bash Here** in CA4/part1B folder                
- ./gradlew clean build                 

**Step6:** Setup a docker file             
- create a txt file in intelliJ                 
- see Dockerfile in part1B                     

**Step7:** To create an image                  
*using **Git Bash Here** in CA4/part1B folder               
- docker build -t ca4part1b .            

**Step8:** Create and run a container                      
*using **Git Bash Here** in CA4/part1B folder                 
- docker run --name container_2 -p 59001:59001 -d ca4part1b                           

**Step9:** Tagging the image and publishing it on Docker Hub                  
*using **Git Bash Here** in CA4/part1B folder         
- docker login                 
- docker tag ca4part1b inacatarina/ca4part1b:1.0              
- docker push inacatarina/ca4part1b:1.0                 

*checked Docker Desktop and my image was pushed to my remote repository*                   

**Step10:** Execute the chat client on the host machine               
*using **Git Bash Here** in DEVOPS/gradle_basic_demo                            
(I had previously cloned repo from git clone https://Catarina_Nogueira@bitbucket.org/luisnogueira/gradle_basic_demo.git)*             
- ./gradlew runClient              

*the chat application is working as intended*                     

**Step11:** Committing files to repository                    
- git add .             
- git commit -m "committing files and updating readme resolves #40"            
- git push            
- git tag ca4-part1b           
- git push origin ca4-part1b                 


