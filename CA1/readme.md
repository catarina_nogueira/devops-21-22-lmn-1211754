# CA1 Repository 

## How to use a Version Control System, a Tutorial

### By Catarina Nogueira


## 1. Using Git and BitBucket, CA1 - Part1

### Part1 only uses the main branch


*First let's start with some info:   
Git is a free and open source distributed version control system;     
Bitbucket is a web-based version control 
repository hosting service for development projects that use Git.  
With this out of the way...*

**Step1:** Start by acessing https://bitbucket.org/ and create an account.  

**Step2:** After creating the account go to the menu repository and select create repository.  

**Step3:** Your repository name should follow these rules: "devops-21-22-teacherName–your number". 

**Step4:** You should also download git from: https://git-scm.com/downloads  

*In the next steps I'll use my own repository for reference.* 

**Step5:** Now, clone your repository into a local folder in your computer:   
- Go to the source menu.    
- Click on the Clone button and copy the link.

*Also, from now on, lets use the option **Git Bash Here** in order to use an emulation of a **bash command line**.   
To use this option, right click in your mouse. It should be there.  
Use this option in any folder you wish to clone a repository.*

**Step6:** To clone your repository into your computer:            
* git clone https://Catarina_Nogueira@bitbucket.org/catarina_nogueira/devops-21-22-lmn-1211754.git 

**Step7:** Also, clone the application "Tutorial React.js and Spring Data REST" into your computer:  
*   git clone https://github.com/spring-guides/tut-react-and-spring-data-rest    

**Step8:** From the bash command line use the command to move the application into your local repository:  
*   cp -R tut-react-and-spring-data-rest/ /d/Switch/devops-21-22-lmn-1211754  

***Attention: when adding another repository into your own never copy the .git folder!***

**Step9:** If you copied it, use the command:   
* rm -R .git 

*This will remove the .git folder*

**Step10:** Now to add the files to the staging area:   
* git add .  

*The added command "." allows to select all edited files to the staging area.  
This prevents adding a file one by one.*

**Step11:** Commit the files      
* git commit -m "First commit" 

*The extra command -m refers to message.  
It allows to add a message at the same time you are using the git commit command, instead of using a text editor.  
This is very helpful you aren't familiar with the commands of a text editor.*  

**Step12:** Followed by:  
* git push

*This commands send the files in the stating area to your remote repository*

**Step13:** Just in case let's check our Git configurations:   
Let's check the email, name and our editor.   
* git config -l  

*This command allows us to verify information associated with our Git*

**Step14:** To change your email or name:  
* git config --global user.name yourName  
or   
* gir config --global user.email yourEmail   

**Step15:** To change your text editor use:  
* git config --global core.editor yourPreferredTextEditor  

*I choose nano.* 

*Let's continue...*   

**Step16:** Create the new directory called CA1.     
If you aren't in your repository, change directories:   
*   cd /d/Switch/devops-21-22-lmn-1211754  

**Step17:** Create the new directory:   
*   mkdir CA1  

**Step18:** Create a readme.md file:   
*   touch readme.md

*You can also create issues in your repository which you can reference via #number in your commits.    
Along the tutorial I have created some issues which I resolved in some commits.*

**Step19:** To commit these changes to the repository:   
*   git add .  

**Step20:** You can also use the command:  
* git status  

*To verify the files added to the staging area.*  

**Step21:** Commit the files to the repository:   
* git commit -m "Created CA1 directory and readme file resolving #1 and #2"   
* git push   

**Step22:** Now, let's copy the same application (Tutorial React.js and Spring Data REST)
into the CA1 folder that we will create in the repository.  
*   cp -R tut-react-and-spring-data-rest CA1/  

**Step23:** Let's add this new directory to our repository:  
* git add .   
* git status   
* git commit -m "added application to repository"  
* git push

**Step24:** Finally, in order to add a tag (which allows us to specify
important moments in your repository history) to our commits use:  
* git tag v1.1.0    

*Since it is the initial version*   

**Step25:** To push to remote:   
*   git push origin v1.1.0

*In the next steps I have added a new feature (jobYears) in application in the CA1 directory.  
All necessary changes were made, and unit tests (server) to validate all parameters  as well.  
These changes were made using the IDE (integrated development environment) intelliJ.  
You can use an IDE of your preference.*

**Step26:** I also tested the client by running the application:  
*   ./mvnw spring-boot:run   

**Step27:** To run the application with the new changes don't 
forget to run the previous command in the correct directory.     
To confirm your current working directory.  
* pwd  


**The working directory should be:**   
*/d/Switch/devops-21-22-lmn-1211754/CA1/tut-react-and-spring-data-rest/basic*

*I also debugged the server using the Chrome extension "React Developer Tools" on the link http://localhost:8080/*

**Step28:** If all works as it should, commit the changes to the repository:  
* git add .  
* git commit -m "Added jobYears feature to application"    
* git push   

**Step29:** I also changed the tag:   
* git tag v1.2.0  
* git push origin v1.2.0

**Step30:** Now, to commit this current readme.md:
* git add .
* git commit -m "Commit current changes to readme.md"
* git push

**Step31:** To change the tag:
* git tag ca1-part1
* git push origin ca1-part1


## 2. Using Git and BitBucket, CA1 - Part2

### Part2: use more than one branch

**Step1:** The main branch of the repository has the stable version of the application
Tutorial React.js and Spring Data REST

**Step2:** Create a new branch named after the new feature to apply:  
* git branch email-field

**Step3:** To view the branches in the repository:
* git branch

*At the moment we have:*
* email-field  
* *main

*To know which branch you're currently on, see the symbol *
This symbol means that your pointer, the head is the main branch*

**Step4:** To change to the email-field branch:
* git switch email-field

*Added the new feature email-field in the application.
Made the necessary changes using the IDE intelliJ.
Added unit tests for the new feature email.*

**Step5:** Commit the changes:
* git add .
* git commit -m "added feature email and corresponding tests"
* git push origin email-field

**Step6:** To return to the main branch
* git switch main

**Step7:** Made changes to readme.md on main
* git add .
* git commit -m "changes to readme.md on main"
* git push
  
**Step8:** To verify the branches log:
* git log --oneline --decorate --graph --all

**Step8:** To merge the email-field branch with the main branch
* git merge email-field

*Previously we switched to the main branch and selected the other branch which we wanted to merge.*

*Because I altered information on the main branch as well, the email-field branch has different information.     
The branches diverged, creating a conflict in the file I edited in different branches.
However, there were no conflicts for me to resolve manually in the merge since Git applied the **"Ort" strategy**, which automatically 
resolves any conflicts.*

**Step9:** Added a tag:
* git tag v1.3.0
* git push origin v1.3.0

**Step10:** To verify our previous commits:
* git log

**Step11** Now, let's create a new branch to a possible bug in the application:          
an invalid email...
* git branch fix-invalid-email

**Step12:** Let's verify the branches:
* git branch

**Step13:** Although we have merged the email-field branch, it still exists.    
Let's delete it:
* git branch -d email-field 

*This deletes the branch in our local repository.    
To also delete it remotely:*
* git push origin --delete email-field

**Step14:** Let's verify the branches again:
* git branch

**Step15:** Switch to the new branch
* git switch fix-invalid-email

*Added another validation for the email field.   
The email must contain the @ character.      
Made the necessary changes and tests as well.*

**Step16:** Commit the changes:
* git add . 
* git commit -m "added another validation for email"
* git push origin fix-invalid email

**Step17** Switch to the main branch 
* git switch main

**Step18:** Verify the branches:
* git branch
* git log --oneline --decorate --graph --all

**Step19** Let's merge the branches
* git merge fix-invalid-email

*This time I had to manually correct the conflicts between branches using the IDE.*

**Step20:** After resolving the merge conflicts don't forget to delete the branch fix-invalid-email.
* git branch -d email-field
* git push origin --delete email-field

**Step21:** Let's add a tag:
* git tag v1.3.1
* git push origin v1.3.1

**Step22:** Implementation of the Git Alternative and final version of the readme file
* git add .
* git commit -m "final version of the readme file CA1-part2"
* git push
* git tag ca1-part2
* git push origin ca1-part2


## 3. Git alternatives

*There are several alternatives to git version control, such as **Azure DevOps Server, 
SVN (subversion), Mercurial**;  
Only to mention a few.      
These alternatives have many similarities with git:*
- They are distributed version control systems (DVCS), meaning:
  - All are systems that records changes to a file or set of files over time so that you can recall specific versions later.
- They are free to use
  - Regarding **Azure DevOps Server** a free version is limited to licensing agreements up to five users; 
more than this requires a paid license.
- **Azure DevOps Server** can be used with a GitHub remote repository.
- Up until 2020, **Mercurial** could be used with Bitbucket. Also, It is free and open-source. It can handle projects of any size and offers an easy and intuitive interface.
- **SVN (subversion)** can be used with GitHub remote repository.
- The alternatives offer similar features to Git such as tags, branches and branch management.
- Alternatives to **Bitbucket** are:
  - Helix Team Hub or Source Forge, to name a few.

*For the alternative implementation I have decided to create a repository in **sourceforge.net** and use **Mercurial** 
as a DVCS, but I would like to further elaborate on some main differences between **Git** and **Mercurial**:*

**Git:**
- Git is a little bit more complex than Mercurial.
- No VCS are entirely secured, but Git offers many functions to enhance safety.
- Git has a powerful and effective branching model. Branching in Git is better than Branching in Mercurial.
- Git supports the staging area, which is known as the index file.
- The most significant benefit with Git is that it has become an industry-standard, which means more developers are familiar with it.
- Git needs periodic maintenance for repositories.
- It holds Linux heritage.
- Git is slightly slower than Mercurial.
- Git supports the unlimited number of parents.

**Mercurial:**
- Mercurial is simpler than Git.
- Mercurial may be safer for fresher users. It has more security features.
- Branching in Mercurial doesn't refer the same meaning as in Git.
- There is no index or staging area before the commit in Mercurial.
- Mercurial's significant benefit is that it's easy to learn and use, which is useful for less-technical content contributors.
- It does not require any maintenance.
- It is python based.
- It is faster than Git.
- Mercurial allows only two parents.

### 3.1 Git alternative implementation

*As stated previously I have created a repository in **Source Forge**, you can access it here:    
https://sourceforge.net/p/devops-21-22-lmn-1211754/code/ci/default/tree/*

*I have also downloaded **Mercurial** here: https://www.mercurial-scm.org/*

*Since most of the main steps are already explained in the 1st and 2nd part of the tutorial, 
I'll be more brief on this 3rd part, implementing both parts at the same time only via readme updates.  
From my understanding, the main purpose of this 3rd part is to present and test the functionality of a Git alternative.    
I also need to add that I managed my repository via the bash command line.*  

*So in the same fashion, I have created my repository and copied the application "Tutorial React.js and Spring Data REST" 
into my newly created repository*

**Step1:** Clone the repository
* hg clone https://catnogueira@hg.code.sf.net/p/devops-21-22-lmn-1211754/code devops-21-22-lmn-1211754-code

**Step2:** Change directory
* cd devops-21-22-lmn-1211754-code

**Step3:** Copied the application into the repository
* cp -R /d/Switch/tut-react-and-spring-data-rest /d/Switch/DEVOPS/devops-21-22-lmn-121175code

**Step4:** Create a readme
* touch readme.txt

**Step5:** Add files to the repository, commit and push to remote
* hg add
* hg commit -m "initial commit"
* hg push

**Step6:** Added a tag and made changes to the readme file
* nano readme.txt
* hg tag v1.1.0 
* hg commit -m "added tag v1.1.0"
* hg push

*To add a tag it is only necessary to use the command's **hg tag** and **hg push.***

**Step7:** Creating a new branch and making changes to the readme file
* hg branch email-field
* nano readme.txt
* hg add
* hg commit -m "changes to readme"
* hg push --new-branch

**Step8:** Switching to default branch and making changes in the readme file
* hg up default
* nano readme.txt
* hg add
* hg commit -m "further changes to readme"
* hg push

**Step9:** Merging email-field branch to default branch
* hg merge email-field
* hg commit -m "merging email-field to default"
* hg push

*A pop-up of the TortoiseHg mercurial GUI appeared to manually resolve the conflict created in the merge.   
Resolving the conflict concluded the merge.*

**Step10:** Added a tag
* hg tag v1.2.0
* hg push

**Step11:** Creating a new branch and making changes to the readme file
* hg branch fix-invalid-email
* nano readme.txt
* hg add
* hg commit -m "changes to readme"
* hg push --new-branch

**Step12:** Switching to default branch and making changes in the readme file
* hg up default
* nano readme.txt
* hg add
* hg commit -m "changes to readme"
* hg push

**Step13:** Merging fix-invalid-email branch to default branch and adding a tag.
* hg merge fix-invalid-email
* hg commit -m "merging fix-invalid-email to default"
* hg tag v1.3.0
* hg push


### 4. Conclusion

**Final thoughts, a comparison on tools used:**

- I didn't find too many differences between using Mercurial and Git since the main commands are very similar.  
However, I did miss the staging area feature of Git and the possibility to verify the status of adding files and commits.     
Still, Mercurial does have a log commit like Git.


- Repository wise, I find Bitbucket much more user-friendly. At first, Source Forge seemed somewhat straight forward but,
it takes a lot of time to verify the changes made locally in the remote repository.     
In this sense, Bitbucket shows any changes immediately, which in my opinion is much easier for a beginner since we are always double-checking our actions.


- I've also verified that Mercurial does not allow deleting branches, as per their info, branches are permanent and 
global.


- Overall, I did not find many differences between the tools, but perhaps since I'm already more at ease Bitbucket (due to previous projects) and more recently with Git, 
I do prefer these tools.
























