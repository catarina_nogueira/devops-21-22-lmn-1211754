package com.greglturnquist.payroll;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    void getFirstName() {
        Employee employee = new Employee("Frodo", "Baggins",
                "Hobbit", "Ring Bearer", 5, "theonering@theshire.net");
        String name = "Frodo";
        String result = employee.getFirstName();
        assertEquals(name, result);
    }

    @Test
    void getFirstNameFailNullFirstName() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            //Arrange
            new Employee(null, "Baggins",
                    "Hobbit", "Ring Bearer", 5, "theonering@theshire.net");
        });
        //Assert
        assertEquals("First name cannot be null.", thrown.getMessage());
    }

    @Test
    void getLastName() {
        Employee employee = new Employee("Frodo", "Baggins",
                "Hobbit", "Ring Bearer", 5, "theonering@theshire.net");
        String name = "Baggins";
        String result = employee.getLastName();
        assertEquals(name, result);
    }

    @Test
    void getLastNameFailNullLastName() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            //Arrange
            new Employee("Frodo", null,
                    "Hobbit", "Ring Bearer", 5, "theonering@theshire.net");
        });
        //Assert
        assertEquals("Last name cannot be null.", thrown.getMessage());
    }

    @Test
    void getDescription() {
        Employee employee = new Employee("Frodo", "Baggins",
                "Hobbit", "Ring Bearer", 5, "theonering@theshire.net");
        String description = "Hobbit";
        String result = employee.getDescription();
        assertEquals(description, result);
    }

    @Test
    void getDescriptionFailNullDescription() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            //Arrange
            new Employee("Frodo", "Baggins",
                    null, "Ring Bearer", 5, "theonering@theshire.net");
        });
        //Assert
        assertEquals("Description cannot be null.", thrown.getMessage());

    }

    @Test
    void getJobTitle() {
        Employee employee = new Employee("Frodo", "Baggins",
                "Hobbit", "Ring Bearer", 5, "theonering@theshire.net");
        String jobTitle = "Ring Bearer";
        String result = employee.getJobTitle();
        assertEquals(jobTitle, result);
    }

    @Test
    void getJobTitleFailNullJobTitle() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            //Arrange
            new Employee("Frodo", "Baggins",
                    "Hobbit", null, 5, "theonering@theshire.net");
        });
        //Assert
        assertEquals("Job title cannot be null.", thrown.getMessage());
    }

    @Test
    void getJobYears() {
        Employee employee = new Employee("Frodo", "Baggins",
                "Hobbit", "Ring Bearer", 5, "theonering@theshire.net");
        int jobYears = 5;
        int result = employee.getJobYears();
        assertEquals(jobYears, result);
    }

    @Test
    void getJobYearsFailZeroYears() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            //Arrange
            new Employee("Frodo", "Baggins",
                    "Hobbit", "Ring Bearer", 0,"theonering@theshire.net");
        });
        //Assert
        assertEquals("Job years cannot be zero nor negative.", thrown.getMessage());

    }

    @Test
    void getJobYearsFailNegativeYears() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            //Arrange
            new Employee("Frodo", "Baggins",
                    "Hobbit", "Ring Bearer", -1, "theonering@theshire.net");
        });
        //Assert
        assertEquals("Job years cannot be zero nor negative.", thrown.getMessage());
    }

    @Test
    void getEmail() {
        Employee employee = new Employee("Frodo", "Baggins",
                "Hobbit", "Ring Bearer", 5, "theonering@theshire.net");
        String expected = "theonering@theshire.net";
        String result = employee.getEmail();
        assertEquals(expected, result);
    }

    @Test
    void getEmailFailNullEmail() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            //Arrange
            new Employee("Frodo", "Baggins",
                    "Hobbit", "Ring Bearer", 5, null);
        });
        //Assert
        assertEquals("Email cannot be null.", thrown.getMessage());
    }

    @Test
    void getEmailFailEmailDoesntContainAtCharacter() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            //Arrange
            new Employee("Frodo", "Baggins",
                    "Hobbit", "Ring Bearer", 5, "theoneringtheshire.net");
        });
        //Assert
        assertEquals("Email must contain @ character.", thrown.getMessage());
    }


}