# CA2/Part2 - Gradle Practice

## Converting the basic version of the Tutorial application to Gradle

### By Catarina Nogueira

*Commands are run using **Git Bash Here** except if another tool is mentioned*

**Step1:** Use **Git Bash Here** in folder 
/c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA2

**Step2:** Create part2 folder                
- mkdir part2

**Step3:** Create readme file                                     
- cd /c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA2/part2                               
- touch readme.md

**Step4:** Commit changes to repository                                               
- git add .                                       
- git commit -m "Create part2 folder and readme.md"                                 
- git push                                  

**Step5:** Additional commit                                   
- git add .                                    
- git commit -m "changes to readme file (resolves #17)"                                              
- git push                                                     
                                     
### Point1                                                          
**Step6:** Create new branch tut-basic-gradle and commit it to repository                                       
- git branch tut-basic-gradle                                           

**Step6:** Change from main branch to tut-basic-gradle branch                    
-git switch tut-basic-gradle                              

**Step7:** Commit new branch to remote                             
- git push --set-upstream origin tut-basic-gradle                                    
                                           
### Point2                                                    
**Step8:** Per the instructions on the CA2, part2 pdf used the link https://start.spring.io/ to start a new gradle sprint boot project                                                   
                                      
### Point3                                
**Step9:** Extract the generated zip file inside the folder "CA2/part2/"                                       

**Step10:** Per the instructions on the pdf ran the following command to verify the available gradle tasks:                               
- ./gradlew tasks                                                     
                                                
### Point4                                     
**Step11:** Delete the src folder                                           
- cd /c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA2/part2/react-and-spring-data-rest-basic                     
- rm -R src/                               

**Step12:** Commit changes                                        
- git add .                                          
- git commit -m "add new gradle spring boot project with src folder deleted (resolves #18, #19, #20, #21)"                        
- git push                                  
                            
### Point5                                     
**Step13:** Copy src folder from the tut-react-and-spring-data-rest application into react-and-spring-data-rest-basic folder                             
- cp -r ~/Documents/Switch/DEVOPS/tut-react-and-spring-data-rest/basic/src/ ~/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA2/part2/react-and-spring-data-rest-basic                         

**Step14:** Copy the package.json and webpack.config.js files                             
- cd ~/Documents/Switch/DEVOPS/tut-react-and-spring-data-rest/basic/src/                           
- cp package.json ~/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA2/part2/react-and-spring-data-rest-basic                              
- cp webpack.config.js ~/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA2/part2/react-and-spring-data-rest-basic                                       

**Step15:** Delete the folder src/main/resources/static/built                             
- because I have made a clone from the original application tut-react-and-spring-data-rest in order to copy 
it to the folder react-and-spring-data-rest-basic, I do not have a built folder to delete                           
                                
### Point6                                      
**Step16:** Run the application:                           
- ./gradlew bootRun                                
- used the link http://localhost:8080/ and it showed empty as it should                                 
                               
### Point7 and 8                              
**Step17:** Added the gradle plugin org.siouan.frontend in the build.gradle file using intelliJ                            
- added the following code (see line 5):                          
    - id "org.siouan.frontend-jdk11" version "6.0.0"                                      

**Step18:** Commit changes                             
- git add .                                                  
- git commit -m "added the gradle plugin org.siouan.frontend (resolves #22, #23)"                                             
- git push                                  
                                       
### Point9                                   
**Step19:** Add the following script to build.gradle file (see line 24 to 29):                               
- frontend {                           
  nodeVersion = "14.17.3"                                
  assembleScript = "run build"                             
  cleanScript = "run clean"                                 
  checkScript = "run check"                                       
  }             
                                             
### Point10                                      
**Step20:** Updated the scripts section in the package.json file (see line 27 to 34) :                             
- "scripts": {                        
  "webpack": "webpack",                    
  "build": "npm run webpack",                         
  "check": "echo Checking frontend",                      
  "clean": "echo Cleaning frontend",                       
  "lint": "echo Linting frontend",                     
  "test": "echo Testing frontend"                        
  },                         
                                      
### Point11                    
**Step22:** Run command:                     
- ./gradlew build                                 

*the build failed when attempting to run it*                                     

**Step21:** Due to the previous implementation it was necessary to delete the build folder due to a build failed error                           
- rm -R ~/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA2/part2/react-and-spring-data-rest-basic/build                                       
            
**Step22:** Run following command in order to delete the build directory:                        
- ./gradlew clean build                                 

**Step23:** Execute the command:                                       
- ./gradlew build                                
                                      
### Point12                         
**Step24:** Run the application:                                
- ./gradlew bootRun                       
- use link http://localhost:8080/                          
- frontend now shows information as it should                                     
                                                     
### Point13                    
**Step25:** Added a task that copies the generated jar files to a folder named "dist"                             
(see line 35 to 38 of the build.gradle file).                                        
Ran the following command in order to verify that the files were copied:                                 
- ./gradlew copyJar                        
                                          
### Point14                              
**Step26:** Added a task to delete all the files generated by the webpack                         
(see line 40 to 44 of the build.gradle file).                               
Ran the following command in order to verify that the files were deleted:                      
- ./gradlew clean                       
                                                   
### Point15                         
**Step27:** Experimenting the created features using the IntelliJ terminal                   
- ./gradlew build                
- ./gradlew copyJar                
- ./gradlew clean                     

- features are working as intended                         

**Step28:** Committing changes to branch                 
- git add .                       
- git commit -m "adding previous tasks to repository (resolves #24, #25, #26)"                   
- git push                               

**Step29:** Merging tut-basic-gradle branch and deleting it                          
- git switch main                         
- git merge tut-basic-gradle                    
- git branch -d tut-basic-gradle                             

*git automatically used the "ort" strategy*                               

### Point16       
**Step30:** Updating readme                                   
- git add .                    
- git commit -m "updating readme"                      
- git push                                            