# CA2/Part2 - Alternative implementation

## Converting the basic version of the Tutorial application to Maven

### By Catarina Nogueira

## 1. Introduction to Gradle and Maven

## 1.1 Gradle

**Gradle** is a build automation tool known for its flexibility to build software.   
A build automation tool is used to automate the creation of applications.  
The building process includes compiling, linking, and packaging the code.  
The process becomes more consistent with the help of build automation tools.   
Gradle provides building, testing, and deploying software on several platforms.

- Gradle is used to build automation in the following languages:                      
  - Java, Scala, Android, C/C++ and Groovy.

**Advantages of using Gradle:**                     
- Gradle resolves all the issues faced on other build tools like Maven and ANT.                       
- The tool focuses on maintainability, usability, extendibility, performance, and flexibility.                     
- It is well-known to be highly customizable when it comes to different projects dealing with various technologies.                       
  We may use Gradle in several ways, like Java projects, Android projects, and Groovy projects.                    
- Gradle is popular to provide high-speed performance, nearly twice as fast as Maven.                             
- The tools support a wide variety of IDE's, which provide a better user experience, 
as different people prefer working on a different IDE. 
It provides the users that like to work on the terminal with the command-line interface,
which offers features like Gradle tasks, Command line completion, etc.

**Disavantages of using Gradle:**                      
-  IDE integration is not as good in Gradle.                                
- Gradle doesn’t come with built-in ant project structure.
  As we can use any build structure for our projects, new programmers find it hard to know the project
  struct and build scripts.
                     
## 1.2 Maven                         
               
**Maven** is a popular open-source build tool developed by the Apache Group to build, publish, 
and deploy several projects at once for better project management.                           
The tool provides allows developers to build and document the lifecycle framework.                           
Maven is based on the Project Object Model (POM), which is an XML file that has all the information
regarding project and configuration details.                                      
The POM has the description of the project, details regarding the versioning,
and configuration management of the project.                               
Maven is chiefly used for Java-based projects, helping to download dependencies,
which refers to the libraries or JAR files.                                        
The tool helps get the right JAR files for each project as there may be different versions of
separate packages.                           
                
- Maven is written in Java and is used to build projects written in:                   
  - C#, Scala, Ruby, etc.                   

**Advantages of using Maven:**                        
- Helps manage all the processes, such as building, documentation, releasing,
  and distribution in project management.                   
- Simplifies the process of project building.                      
- Increases the performance of the project and the building process.                     
- The task of downloading Jar files and other dependencies is done automatically.                         
- Provides easy access to all the required information.                                 
- Makes it easy for the developer to build a project in different environments without worrying about
  the dependencies, processes, etc.                            
- In Maven, it’s easy to add new dependencies by writing the dependency code in the pom file.                     

**Disavantages of Maven:**                         
- Maven requires installation in the working system and the Maven plug-in for the IDE.                           
- If the Maven code for an existing dependency is unavailable,
  you cannot add that dependency using Maven itself.                       
- Some sources claim that Maven is slow.                              

## 1.3 Gradle vs Maven                                    

| Gradle                                                                         | Maven | 
|--------------------------------------------------------------------------------|---------------------------------------------|
| Build automation system using Groovy-based Domain Specific Language            | Software Project Management system used for Java projects
| Doesn't use an XML file for the declaration of project configuration           | Uses an XML file for declaring the project and its dependencies
| The goal is to add functionality to a project.                                 | The goal is related to a project phase
| Based on a graph of task dependencies.                                         | Based on the phases of the linear and fixed model
| Works only on the tasks that have been changed to give a better performance    | Does not use build-cache, making the build time slower
| Highly customizable, thus providing a broad range of IDE support builds 		 | Provides a limited number of parameters and requirements     

              
## 2. Alternative implementation                  

**There are some steps that I'll skip in this implementation, for example,
creating a new branch (#Point1) since I have already implemented this step on the CA2-part2 tutorial.**                 

*Also, commands are run using **Git Bash Here** except if another tool is mentioned*                     

**Step1:** Use **Git Bash Here** in folder /c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA2

**Step2:** Create part2 folder                        
- mkdir part2-Alternative

**Step3:** Create readme file                         
- cd /c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA2/part2-Alternative                                        
- touch readme.md

**Step4:** Commit changes to repository                            
- git add .                    
- git commit -m "Create part2 folder and readme.md"                             
- git push                     
             
### Point2                             
**Step5:** Per the instructions on the CA2, part2 pdf used the link https://start.spring.io/ 
to start a new sprint boot project, this time with Maven
               
### Point3                              
**Step6:** Extract the generated zip file inside the folder "CA2/part2-Alternative/"                    

### Point4                     
**Step7:** Delete the src folder                          
- cd /c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA2/part2-Alternative/react-and-spring-data-rest-basic                   
- rm -R src/                         

### Point5                           
**Step8:** Copy src folder from the tut-react-and-spring-data-rest application into react-and-spring-data-rest-basic folder                        
- cp -r ~/Documents/Switch/DEVOPS/tut-react-and-spring-data-rest/basic/src/ ~/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA2/part2-Alternative/react-and-spring-data-rest-basic                         

**Step9:** Copy the package.json and webpack.config.js files               
- cd ~/Documents/Switch/DEVOPS/tut-react-and-spring-data-rest/basic/src/                 
- cp package.json ~/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA2/part2-Alternative/react-and-spring-data-rest-basic                 
- cp webpack.config.js ~/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA2/part2-Alternative/react-and-spring-data-rest-basic                     

**Step10:** Delete the folder src/main/resources/static/built                      
- because I have made a clone from the original application tut-react-and-spring-data-rest in order to copy
  it to the folder react-and-spring-data-rest-basic, I do not have a built folder to delete                      

### Point6                 
**Step11:** Run the application:                
- ./mvnw spring-boot:run                                                                
- used the link http://localhost:8080/ and it showed empty as it should                                                                          
                   
### Point7, 8 and 9                               
**Step12:** Checked the Maven the following github, in order to add
the frontend-maven-plugin to the project:                      
- https://github.com/eirslett/frontend-maven-plugin                        
- Added the plugin (see line 51 to 92 from the pom.xml file), its execution and configuration                 

### Point10                           
**Step13:** Update the scripts section in package.json add:               
- see package.json file (line 27 to 34)                     

### Point11               
**Step14:** Execute the project build:                       
- ./mvnw install                    

### Point12:                      
**Step15:** Run the application:                         
- ./mvnw spring-boot:run                
- used the link http://localhost:8080/ and it works as it should                        
                 
**Step16:** Committing changes                        
- git add .                         
- git commit -m "alternative implementation up to point #12"                         
- git push                                  

### Point13                                             
**Step17:** Added a task that copies the generated jar files to a folder named "dist":                    
 - see pom.xml file (line 95 to 119)                          

### Point14                  
**Step18:** Added a task to delete all the files generated by the webpack                               
- see pom.xml file (line 121 to 132)                    

### Point15                    
**Step19:** Experimenting the created features:                        
- ./mvnw install (runs the build and copy plugin; works as intended)                         
- ./mvnw clean (invokes just clean and clean plugin to delete the static/built folder; works as intended)                       

### Point16                                  
**Step20:** Committing changes            
- git add .                         
- git commit -m "alternative implementation continuation"                     
- git push   

### Point17                                              
**Step21:** Completed alternative implementation, committing changes to the repository, adding ca2-part2 tag                      
- git add .                      
- git commit -m "committing completed readme. (resolves #27)"                        
- git push                    
- git tag ca2-part2                     
- git push origin ca2-part2                                 

## Conclusion                              

*Overall, I found it easier to work with **Gradle**.                                    
For a beginner like me, 
more at ease with Java programming language, I found Groovy 
(Apache Groovy language) easier and more straightforward to apply, making task creation
and implementation more direct.                            
On the other hand, XML (extensible markup language) doesn't seem to be as 
readable and easy to implement.*

*Still, both **Maven** and **Gradle** seemed very flexible extension wise, in the sense that in order to expand
its "tasks" or "plugins" we only need to add the needed script in the file.                     
Although, in **Maven** the script needs to be inside particular tags, 
this is not true for **Gradle**.                          
Due to this, I do find that **Gradle** 
is more straightforward than **Maven** or at least more direct in its approach 
to configuration and extension.                    
We also need to take into account the difficulty of the tasks requested in 
this assignment; the implementation of complex tasks might create different 
challenges for the built tools.*

*Also, the main difference I found between the built tools was that **Gradle** allows
for tasks to be run independently, while **Maven** must compile most of its scripts at once.                         
With **Maven** we need to run install command (which runs its build) or the clean command (which cleans its build).*

*Lastly, I would say that I indeed preferred using **Gradle** in this assignment, 
rather than **Maven**.                                         
I found it to be more easy and flexible, at least regarding
tasks creation and compilation.                                    
In my opinion, it would be easier to integrate and 
add new tasks to this built tool and run custom tasks as needed.*